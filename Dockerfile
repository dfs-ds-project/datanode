FROM python:3.7.5

RUN pip install --upgrade pip
RUN apt-get update && apt-get install -y --no-install-recommends gettext && apt-get clean
RUN apt-get install -y netcat

EXPOSE 5000

WORKDIR /app
COPY . .

RUN pip install -r requirements.txt

CMD sh ./wait-for-namenode.sh && gunicorn -w 1 -b 0.0.0.0:5000 --log-level debug datanode.wsgi:app

