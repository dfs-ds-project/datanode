# -*- coding: utf-8 -*-

from os import path
from shutil import move, copy
from typing import Tuple, Dict, Union
from datetime import datetime

from flask import Blueprint, request, current_app, send_file

from . import controllers
from .schemas import UploadFile, MoveFile
from ...helpers import (
    md5_check_sum,
    get_path,
    marshmallow_exception_handler,
    request_logging,
)

blue_print = Blueprint('files', __name__, url_prefix='/api/filesystem/files')


@blue_print.route("/", methods=['POST'])
@marshmallow_exception_handler
@request_logging
def create_file() -> Tuple[Dict, int]:
    file_path = UploadFile().load(request.form)['path']
    full_path = get_path(file_path)
    file_object = request.files.get('file', None)
    if file_object:
        file_check_sum = controllers.save_file(file_object, file_path)
    else:
        open(full_path, 'w').close()
        file_check_sum = md5_check_sum(full_path)
        current_app.files[file_path] = file_check_sum

    return {'file_check_sum': file_check_sum}, 201


@blue_print.route("/", methods=['GET'])
@request_logging
def read_file():
    file_path = request.args.get('path', None)
    if file_path and path.exists(get_path(file_path)):
        return send_file(get_path(file_path)), 200
    return '', 404


@blue_print.route("/", methods=['DELETE'])
@request_logging
def remove_file() -> Tuple[str, int]:
    file_path = request.args.get('path', None)
    if file_path and path.exists(get_path(file_path)):
        controllers.remove_file(file_path)
        return '', 204
    return '', 404


@blue_print.route("/", methods=['PATCH'])
@marshmallow_exception_handler
@request_logging
def move_file() -> Tuple[str, int]:
    file_path = request.args.get('path', None)
    new_path = MoveFile().load(request.form)['new_path']
    if file_path and path.exists(get_path(file_path)):
        move(get_path(file_path), get_path(new_path))
        current_app.files[new_path] = current_app.files.pop(file_path)
        return '', 200
    return '', 404


@blue_print.route("/", methods=['PUT'])
@marshmallow_exception_handler
@request_logging
def copy_file() -> Tuple[str, int]:
    file_path = request.args.get('path', None)
    new_path = MoveFile().load(request.form)['new_path']
    if file_path and path.exists(get_path(file_path)):
        copy(get_path(file_path), get_path(new_path))
        current_app.files[new_path] = current_app.files[file_path]
        return '', 200
    return '', 404


@blue_print.route("/info/", methods=['GET'])
@request_logging
def file_info() -> Tuple[Union[Dict, str], int]:
    file_path = request.args.get('path', None)
    if file_path and path.exists(get_path(file_path)):
        return (
            {
                'size': int(path.getsize(get_path(file_path))),
                'last_modification': datetime.utcfromtimestamp(
                    path.getmtime(get_path(file_path))
                ).strftime('%Y-%m-%d %H:%M:%S'),
            },
            200,
        )
    return '', 404
