# -*- coding: utf-8 -*-

from marshmallow import Schema
from marshmallow.fields import String

from ...helpers import validate_path


class UploadFile(Schema):
    path = String(required=True, validate=validate_path)


class MoveFile(Schema):
    new_path = String(required=True, validate=validate_path)
