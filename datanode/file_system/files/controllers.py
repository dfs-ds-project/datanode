# -*- coding: utf-8 -*-

from os import remove

from flask import current_app

from ...helpers import md5_check_sum, get_path


def save_file(file_object, file_path) -> str:
    file_object.save(get_path(file_path))
    file_check_sum = md5_check_sum(get_path(file_path))
    current_app.files[file_path] = file_check_sum
    return file_check_sum


def remove_file(file_path):
    remove(get_path(file_path))
    current_app.files.pop(file_path, None)
