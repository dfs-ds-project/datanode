# -*- coding: utf-8 -*-

from os import path
from typing import Tuple

from flask import Blueprint, request

from . import controllers
from .schemas import CreateDirectory
from ...helpers import marshmallow_exception_handler, get_path, request_logging

blue_print = Blueprint('dirs', __name__, url_prefix='/api/filesystem/dirs')


@blue_print.route("/", methods=['POST'])
@marshmallow_exception_handler
@request_logging
def create_dir() -> Tuple[str, int]:
    dir_path = CreateDirectory().load(request.form)['path']
    if not path.exists(get_path(dir_path)):
        controllers.create_dir(dir_path)
        return '', 201
    return '', 400


@blue_print.route("/", methods=['DELETE'])
@marshmallow_exception_handler
@request_logging
def remove_dir() -> Tuple[str, int]:
    dir_path = request.args.get('path', None)
    if dir_path and path.exists(get_path(dir_path)):
        controllers.remove_dir(dir_path)
        return '', 200
    return '', 404
