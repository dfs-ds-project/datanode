# -*- coding: utf-8 -*-

from shutil import rmtree
from os import mkdir

from flask import current_app

from ...helpers import get_path


def remove_dir(dir_path: str) -> None:
    rmtree(get_path(dir_path))
    current_app.dirs = list(
        filter(
            lambda curr_dir: not curr_dir.startswith(dir_path),
            current_app.dirs,
        )
    )
    current_app.files = {
        key: current_app.files[key]
        for key in current_app.files
        if not key.startswith(dir_path)
    }


def create_dir(dir_path: str) -> None:
    mkdir(get_path(dir_path))
    current_app.dirs.append(dir_path)
