# -*- coding: utf-8 -*-

from marshmallow import Schema
from marshmallow.fields import String

from ...helpers import validate_path


class CreateDirectory(Schema):
    path = String(required=True, validate=validate_path)
