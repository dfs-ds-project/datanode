# -*- coding: utf-8 -*-

from hashlib import md5
from os import path
from functools import wraps

from marshmallow import ValidationError
from flask import current_app, request


def md5_check_sum(file_name: str) -> str:
    hash_md5 = md5()
    with open(file_name, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()


def get_path(relative_path: str) -> str:
    return path.join(current_app.config['DATA_DIR'], relative_path)


def marshmallow_exception_handler(handler):
    @wraps(handler)
    def wrap(*args: list, **kwargs: dict):
        try:
            return handler(*args, **kwargs)
        except ValidationError as exception:
            return exception.normalized_messages(), 400

    return wrap


def request_logging(handler):
    @wraps(handler)
    def wrap(*args: list, **kwargs: dict):
        current_app.logger.debug("Args: " + str(request.args))
        current_app.logger.debug("JSON: " + str(request.json))
        current_app.logger.debug("Form: " + str(request.form))
        return handler(*args, **kwargs)
    return wrap


def validate_path(value: str) -> None:
    current_app.logger.debug(get_path(value))
    current_app.logger.debug(get_path('/'.join(value.split('/')[:-1])))
    if not path.exists(get_path('/'.join(value.split('/')[:-1]))):
        raise ValidationError('Path does not exist')
