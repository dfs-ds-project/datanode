# -*- coding: utf-8 -*-

from marshmallow import Schema
from marshmallow.fields import List, String, Dict


class UpdateMetadata(Schema):
    dirs = List(String(), required=True)
    files = Dict(String(), Dict(String(), String()), required=True)