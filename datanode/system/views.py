# -*- coding: utf-8 -*-

from os import mkdir, walk, path
from shutil import rmtree, disk_usage
from typing import Tuple, Dict

from flask import Blueprint, current_app, request
from requests import get

from .schemas import UpdateMetadata
from ..file_system.dirs.controllers import create_dir, remove_dir
from ..file_system.files.controllers import remove_file
from ..helpers import (
    marshmallow_exception_handler,
    request_logging,
    get_path,
    md5_check_sum,
)

GET_FILE_URL = '{0}/api/filesystem/files/?path={1}'

blue_print = Blueprint('system', __name__, url_prefix='/api/system')


def download_file(dfs_file_path, host):
    open(get_path(dfs_file_path), 'wb').write(
        get(GET_FILE_URL.format(host, dfs_file_path)).content
    )


@blue_print.route("/init/", methods=['GET'])
def init_datanode() -> Tuple[Dict, int]:
    current_app.files = {}
    current_app.dirs = []
    rmtree(current_app.config['DATA_DIR'], ignore_errors=True)
    mkdir(current_app.config['DATA_DIR'])
    return {'available_size': int(disk_usage('/')[2])}, 200


@blue_print.route("/ping/", methods=['GET'])
def ping() -> Tuple[Dict, int]:
    return {'files': current_app.files, 'dirs': current_app.dirs}, 200


@blue_print.route("/update/", methods=['POST'])
@marshmallow_exception_handler
@request_logging
def update_metadata():
    valid_dirs = UpdateMetadata().load(request.json)['dirs']
    valid_files = UpdateMetadata().load(request.json)['files']

    for root, subdirs, files in walk(get_path('')):
        for d in subdirs:
            dfs_dir_path = path.join(root, d).replace(get_path(''), '')
            if dfs_dir_path not in valid_dirs:
                remove_dir(dfs_dir_path)
            else:
                if dfs_dir_path not in current_app.dirs:
                    current_app.dirs.append(dfs_dir_path)
        for f in files:
            dfs_file_path = path.join(root, f).replace(get_path(''), '')
            if dfs_file_path not in valid_files.keys():
                remove_file(dfs_file_path)
            else:
                if (
                    md5_check_sum(get_path(dfs_file_path))
                    != valid_files[dfs_file_path]['file_check_sum']
                ):
                    download_file(
                        dfs_file_path, valid_files[dfs_file_path]['host']
                    )
                current_app.files[dfs_file_path] = md5_check_sum(
                    get_path(dfs_file_path)
                )

    for dir_for_creation in sorted(
        set(valid_dirs) - set(current_app.dirs), key=lambda d: len(d)
    ):
        create_dir(dir_for_creation)
    for file_for_creation in set(valid_files.keys()) - set(
        current_app.files.keys()
    ):
        download_file(
            file_for_creation, valid_files[file_for_creation]['host']
        )
        current_app.files[file_for_creation] = md5_check_sum(
            get_path(file_for_creation)
        )

    return '', 200
