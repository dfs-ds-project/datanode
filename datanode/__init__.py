# -*- coding: utf-8 -*-

import socket
from os import environ
from os.path import exists

import requests
from flask import Flask

from .config import development


def create_app():
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object('datanode.config.development')
    app.files = {}
    app.dirs = []

    host_name = socket.gethostname()
    host_ip = socket.gethostbyname(host_name)

    try:
        requests.post(
            'http://'
            + environ.get('NAMENODE_HOST')
            + ':8080/system/datanodes/',
            json={
                'host': host_ip + ':5000',
                'restored': exists(development.DATA_DIR),
            },
            timeout=0.00001
        )
    except Exception:
        pass

    from .system import views as system_views
    from .file_system.files import views as files_views
    from .file_system.dirs import views as dirs_views

    app.register_blueprint(system_views.blue_print)
    app.register_blueprint(files_views.blue_print)
    app.register_blueprint(dirs_views.blue_print)

    return app
